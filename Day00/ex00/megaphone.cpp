/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/06 12:09:16 by matthieu          #+#    #+#             */
/*   Updated: 2021/11/09 13:58:41 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cctype>
#include <cstring>
#include <iostream>

int	main(int ac, char **av)
{
	int i;

	for (i = 1; i < ac; i++)
	{
		for (int j = 0; j != (int)strlen(av[i]); j++)
			std::cout << (char)toupper(av[i][j]);
	}
	if (i == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	std::cout << std::endl;
	return (0);
}
