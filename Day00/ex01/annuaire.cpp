/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   annuaire.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 18:06:16 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/10 21:32:45 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdio.h>
#include "Phonebook.hpp"
#include <stdlib.h>

int		ft_min(int a, int b)
{
	if (a < b)
		return a;
	return b;
}

int	main(void)
{
	Phonebook	annuaire;
	bool		stop = 0;
	char		input[128];
	int			i = 0;

	while (!stop)
	{
		std::cin >> input;
		if (!strcmp(input, "EXIT"))
			stop = 1;
		else if (!strcmp(input, "ADD"))
		{
			if (i >= 8)
				std::cout << "PhoneBook full, overwriting..."<< std::endl;
			annuaire.add();
			i++;
		}
		else if (!strcmp(input, "SEARCH") && i)
		{
			std::cout << "┌──────────┬──────────┬──────────┬──────────┐" << std::endl;
			std::cout << "│" << std::setw(10) << "index" << "│" << std::setw(10) << "First Name" << "│" << std::setw(10) << "Last Name" << "│" << std::setw(10) << "Nickname" << "│" << std::endl;
			std::cout << "├──────────┼──────────┼──────────┼──────────┤" << std::endl;
			for (int j = 0; j < ft_min(i, 8); j++)
			{
				if (j)
					std::cout << "├──────────┼──────────┼──────────┼──────────┤" << std::endl;
				std::cout << "│" << std::setw(10) << j + 1 << "│";
				annuaire.quick_show(j);
			}
			std::cout << "└──────────┴──────────┴──────────┴──────────┘" << std::endl;
			std::cout << "Please enter here and now the index number of the contact you want to have informations about right here and we'll give them to you : ";
			std::cin >> input;
			int in = atoi(input);
			if (0 <= in - 1 && in - 1 < ft_min(i, 8))
				annuaire.show(in - 1);
			else
				std::cout<<"Please enter a correct answer !" << std::endl;
		}
	}
	return (0);
}
