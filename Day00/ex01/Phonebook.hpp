/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/10 20:35:43 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 14:35:21 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

# include "Contacts.hpp"

class Phonebook {
	public:
		Phonebook(void);
		Phonebook(Phonebook const &src);
		virtual ~Phonebook(void);

		void	add(void);
		void	show(int id) const;
		void	quick_show(int id) const;

		void		setContact(int id);
		Contacts	getContact(int id) const;

	private:
		int			_id;
		Contacts	_phonebook[8];
};

#endif