/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/10 20:33:00 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 14:36:08 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Phonebook.hpp"
#include <string.h>
#include <iostream>
#include <iomanip>

Phonebook::Phonebook(void)
{
	_id = 0;
}
Phonebook::~Phonebook(void) {}

Contacts	Phonebook::getContact(int id) const
{
	return _phonebook[id];
}
void	Phonebook::add(void)
{
	std::string tmp;

	std::cout << "Please enter contact Informations :" << std::endl;
	std::cout << "Fisrt Name : ";
	std::cin >> tmp;
	_phonebook[_id%8].set_first_name(tmp);
	std::cout << "Last Name : ";
	std::cin >> tmp;
	_phonebook[_id%8].set_last_name(tmp);
	std::cout << "Nickname : ";
	std::cin >> tmp;
	_phonebook[_id%8].set_nickname(tmp);
	std::cout << "Phone Number : ";
	std::cin >> tmp;
	_phonebook[_id%8].set_phone_number(tmp);
	std::cout << "Darkest Secret : ";
	std::cin >> tmp;
	_phonebook[_id%8].set_darkest_secret(tmp);
	std::cout << "Contact succesfully added !" << std::endl;
	_id++;
}

void	Phonebook::show(int id) const
{
	std::cout << "First Name :.... " << _phonebook[id].get_first_name() << std::endl;
	std::cout << "Last Name :..... " << _phonebook[id].get_last_name() << std::endl;
	std::cout << "Nickname :...... " << _phonebook[id].get_nickname() << std::endl;
	std::cout << "Phone Number :.. " << _phonebook[id].get_phone_number() << std::endl;
	std::cout << "Darkest Secret : " << _phonebook[id].get_darkest_secret() << std::endl;
}

void	Phonebook::quick_show(int id) const
{
	if (_phonebook[id].get_first_name().length() > 10)
		std::cout <<_phonebook[id].get_first_name().substr(0, 9) << ".│";
	else
		std::cout << std::setw(10) << _phonebook[id].get_first_name() << "│";
	if (_phonebook[id].get_last_name().length() > 10)
		std::cout << _phonebook[id].get_last_name().substr(0, 9) << ".│";
	else
		std::cout << std::setw(10) << _phonebook[id].get_last_name() << "│";
	if (_phonebook[id].get_nickname().length() > 10)
		std::cout << _phonebook[id].get_nickname().substr(0, 9) << ".│" << std::endl;
	else
		std::cout << std::setw(10) << _phonebook[id].get_nickname() << "│" << std::endl;
}