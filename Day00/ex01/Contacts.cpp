/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contacts.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 18:43:52 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 14:21:58 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contacts.hpp"
#include <cstring>
#include <stdio.h>

Contacts::Contacts(void) { return ; }

Contacts::~Contacts(void) { return ; }

std::string	Contacts::get_first_name(void) const { return _first_name; }
std::string	Contacts::get_last_name(void) const { return _last_name; }
std::string	Contacts::get_nickname(void) const { return _nickname; }
std::string	Contacts::get_phone_number(void) const {return _phone_number; }
std::string	Contacts::get_darkest_secret(void) const {return _darkest_secret; }

void	Contacts::set_first_name(std::string str) { _first_name = str; }
void	Contacts::set_last_name(std::string str) { _last_name = str; }
void	Contacts::set_nickname(std::string str) { _nickname = str; }
void	Contacts::set_phone_number(std::string str) { _phone_number = str; }
void	Contacts::set_darkest_secret(std::string str) { _darkest_secret = str; }
