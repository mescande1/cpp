/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/09 13:58:00 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/09 17:00:23 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"
#include <stdlib.h>
#include <iostream>

Base *	generate(void)
{
	Base	*res;
	int		i = rand();

	if (i % 3 == 0)
	{
		std::cout<<"A";
		res = new A;
	}
	if (i % 3 == 1)
	{
		std::cout<<"B";
		res = new B;
	}
	if (i % 3 == 2)
	{
		std::cout<<"C";
		res = new C;
	}
	return res;
}

void identify(Base *p)
{
	if (dynamic_cast<A*>(p))
		std::cout<<"A";
	if (dynamic_cast<B*>(p))
		std::cout<<"B";
	if (dynamic_cast<C*>(p))
		std::cout<<"C";
}

void identify(Base &p)
{
	(void)p;
	try
	{
		Base &tmp = dynamic_cast<A&>(p);
		std::cout<<"A";
		(void)tmp;
	}
	catch (std::exception &e)
	{
		try
		{
			Base &tmp = dynamic_cast<B&>(p);
			std::cout<<"B";
			(void)tmp;
		}
		catch (std::exception &e)
		{
			try
			{
				Base &tmp = dynamic_cast<C&>(p);
				std::cout<<"C";
				(void)tmp;
			}
			catch (std::exception &e)
			{
				std::cout<<"wrongtype";
			}
		}
	}
	std::cout<<std::endl;
}

int main(void)
{
	srand(static_cast<unsigned int>(time(NULL)));
	for (int i = 0; i < 10; i++)
	{
		Base *ptr = generate();
		identify(ptr);
		identify(*ptr);
	}
	return 0;
}
