/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/07 16:07:00 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/12 20:54:59 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include <limits.h>
#include <stdlib.h>
#include <math.h>
#include <cstdio>

bool	isnum(char c)
{
	return ('0' <= c && c <= '9');
}

char	gettype(std::string num)
{
	bool	comma = false;
	long unsigned int	i = 0;
	bool	sign = false;

	while (num[i] == ' ' || num[i] == '-' || num[i] == '+')
	{
		if (num[i] == '-' || num[i] == '+')
		{
			if (sign)
			{
				std::cout<<"Syntax Error"<<std::endl;
				return 0;
			}
			sign = true;
		}
		i++;
	}
	if (!num.compare(i, 5, "inff") || !num.compare(i, 5, "nanf"))
		return 'f';
	if (!num.compare(i, 4, "inf") || !num.compare(i, 4, "nan"))
		return 'd';
	while (i < num.length())
	{
		if (num[i] == '.' || num[i] == ',')
		{
			if (comma)
			{
				std::cout<<"Syntax Error"<<std::endl;
				return 0;
			}
			else
				comma = true;
		}
		else if (!isnum(num[i]))
		{
			if (i == 0 && num.length() == 1)
				return 'c';
			if (num[i] == 'f' && i == num.length() - 1)
				return 'f';
			std::cout<<"Syntax Error"<<std::endl;
			return 0;
		}
		i++;
	}
	if (i == 1 && (comma || sign))
		return 'c';
	if (comma)
		return 'd';
	return 'i';
}

void printc(std::string num)
{
	char c = num[0];
	std::cout<<"char: "<<c<<"\tTHIS IS THE TYPE"<<std::endl;
	std::cout<<"int: "<<static_cast<int>(c)<<std::endl;
	std::cout<<"float: "<<static_cast<float>(c)<<std::endl;
	std::cout<<"double: "<<static_cast<double>(c)<<std::endl;
}

bool	isprintable(char c)
{
	return (32 <= c && c <= 126);
}

std::string		convertchar(int i)
{
	if (i < 0 || 255 < i)
		return "impossible";
	if (!isprintable(static_cast<char>(i)))
		return "Non displayable";
	char c = static_cast<int>(i);
	std::string tmp = "\'";
	tmp.append(&c);
	tmp.append("\'");
	return tmp;
}

void	printi(char *num)
{
	double	d = strtod(num, NULL);
	int		i = atoi(num);

	if (d < INT_MIN || INT_MAX < d || isnan(d))
	{
		std::cout<< "int : impossible\tTHIS IS THE TYPE"<<std::endl;
		return ;
	}
	std::cout<<"char: "<<convertchar(i)<<std::endl;
	std::cout<<"int: "<<i<<"\tTHIS IS THE TYPE"<<std::endl;
	std::cout<<"float: "<<static_cast<float>(i)<<std::endl;
	std::cout<<"double: "<<static_cast<double>(i)<<std::endl;
}

std::string		convertint(float f)
{
	std::string res;
	char *tmp = new char[200];
	sprintf(tmp, "%d", static_cast<int>(f));
	res.append(tmp);
	delete [] tmp;
	return res;
}

void	printf(char *num)
{
	//double d = strtod(num, NULL);
	float f = strtof(num, NULL);

	if (static_cast<float>(INT_MIN) <= f && f <= static_cast<float>(INT_MAX))
	{
		std::cout<<"char: "<<convertchar(static_cast<int>(f))<<std::endl;
		std::cout<<"int: "<<convertint(f)<<std::endl;
	}
	else
	{
		std::cout<<"char: "<<convertchar(-1)<<std::endl;
		std::cout<<"int: impossible"<<std::endl;
	}
	std::cout<<"float: "<<f<<"\tTHIS IS THE TYPE"<<std::endl;
	std::cout<<"double: "<<static_cast<double>(f)<<std::endl;
}

void printd(char *num)
{
	double d = strtod(num, NULL);

	if (static_cast<double>(INT_MIN) <= d && d <= static_cast<double>(INT_MAX))
	{
		std::cout<<"char: "<<convertchar(static_cast<int>(d))<<std::endl;
		std::cout<<"int: "<<convertint(static_cast<float>(d))<<std::endl;
	}
	else
	{
		std::cout<<"char: "<<convertchar(-1)<<std::endl;
		std::cout<<"int: impossible"<<std::endl;
	}
	std::cout<<"float: "<<static_cast<float>(d)<<std::endl;
	std::cout<<"double: "<<d<<"\tTHIS IS THE TYPE"<<std::endl;
}

int		main(int ac, char **av)
{
	if (ac != 2)
	{
		std::cout <<"Wrong number of argument"<<std::endl;
		return 1;
	}
	char type = gettype(av[1]);
	if (type == 'c')
		printc(av[1]);
	else if (type == 'i')
		printi(av[1]);
	else if (type == 'f')
		printf(av[1]);
	else if (type == 'd')
		printd(av[1]);
	return 0;
}
