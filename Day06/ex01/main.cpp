/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/08 17:24:28 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/09 13:29:48 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <stdint.h>

typedef struct s_Data {
	std::string s;
}	Data;

uintptr_t serialize(Data* ptr)
{
	return reinterpret_cast<uintptr_t>(ptr);
}

Data*	deserialize(uintptr_t raw)
{
	return reinterpret_cast<Data *>(raw);
}

int	main()
{
	int		i = 42;
	uintptr_t	rawint = i;
	Data *ptr = deserialize(rawint);
	std::cout<<rawint<<std::endl;
	rawint = serialize(ptr);
	std::cout<<rawint<<std::endl;

	Data		*src_ptr = new Data;

	src_ptr->s = "How are you doing Jimmy ?";
	rawint = serialize(src_ptr);
	ptr = deserialize(rawint);
	std::cout << src_ptr->s << std::endl;
	std::cout << ptr->s << std::endl;
	delete src_ptr;
}
