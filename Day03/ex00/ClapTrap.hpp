/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 01:46:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 09:28:56 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class ClapTrap {
	public:
		ClapTrap(std::string name = "");
		ClapTrap(ClapTrap const &src);
		~ClapTrap(void);

		ClapTrap &	operator=(ClapTrap const &rhs);

		void	attack(std::string const &target) const;
		void	takeDamage(unsigned int amount);
		void	beRepaired(unsigned int amount);

	private:

		std::string		_name;
		unsigned int	_hp;
		unsigned int	_ep;
		unsigned int	_dmg;
};

#endif