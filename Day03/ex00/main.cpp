/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 02:04:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 09:31:13 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

int main(void)
{
	ClapTrap a;
	ClapTrap b("Patrick");
	ClapTrap c("Jose");
	ClapTrap d(b);

	a = c;
	a.takeDamage(42);
	b.beRepaired(10);
	c.takeDamage(5);
	d.takeDamage(5);
	b.attack("Jose");
	return (0);
}
