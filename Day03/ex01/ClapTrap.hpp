/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 01:46:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 09:39:36 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class ClapTrap {
	public:
		ClapTrap(std::string name = "");
		ClapTrap(ClapTrap const &src);
		~ClapTrap(void);

		ClapTrap &	operator=(ClapTrap const &rhs);

		void	attack(std::string const &target) const;
		void	takeDamage(unsigned int amount);
		void	beRepaired(unsigned int amount);

		std::string		getname(void) const;
		unsigned int	gethp(void) const;
		unsigned int	getep(void) const;
		unsigned int	getdmg(void) const;

	protected:

		std::string		_name;
		unsigned int	_hp;
		unsigned int	_ep;
		unsigned int	_dmg;

	private:
};

std::ostream	&operator<<(std::ostream &o, ClapTrap const &rhs);

#endif