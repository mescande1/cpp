/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 02:04:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 09:50:57 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

int main(void)
{
	ScavTrap a;
	ScavTrap b("Patrick");
	ScavTrap c("Jose");
	ScavTrap d(b);

	std::cout<<a<<std::endl;
	a = c;
	std::cout<<a<<std::endl;
	a.beRepaired(10);
	std::cout<<a<<std::endl;
	a.guardGate();
	std::cout<<a<<std::endl;
	return (0);
}
