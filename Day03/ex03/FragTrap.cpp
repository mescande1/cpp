/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 14:38:01 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 13:49:48 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(std::string name) : ClapTrap(name)
{
	std::cout << "Default FragTrap constructor called" << std::endl;
	setdefaulthp();
	setdefaultep();
	setdefaultdmg();
}
FragTrap::FragTrap(FragTrap const &src) : ClapTrap(src)
{
	std::cout << "Copy FragTrap constructor called" << std::endl;
	*this = src;
}
FragTrap::~FragTrap(void)
{
	std::cout << "Default FragTrap Destructor called" << std::endl;
}

FragTrap &	FragTrap::operator=(FragTrap const &rhs)
{
	ClapTrap::operator=(rhs);
	return *this;
}

std::ostream	&operator<<(std::ostream &o, FragTrap const &rhs)
{
	o << ClapTrap(rhs);
	return o;
}

void	FragTrap::highFiveGuys(void)
{
	std::cout << "+ High Five ? :D" << std::endl;
}

void	FragTrap::setdefaulthp(void) { _hp = 100; }
void	FragTrap::setdefaultep(void) { _ep = 100; }
void	FragTrap::setdefaultdmg(void) { _dmg = 30; }