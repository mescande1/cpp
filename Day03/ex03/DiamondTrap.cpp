/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DiamondTrap.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 17:26:18 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 13:51:18 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DiamondTrap.hpp"

DiamondTrap::DiamondTrap(std::string name) : ClapTrap(std::string(name).append("_clap_name")), _name(name)
{
	std::cout << "Default DiamondTrap constructor called" << std::endl;
	FragTrap::setdefaulthp();
	ScavTrap::setdefaultep();
	FragTrap::setdefaultdmg();
}
DiamondTrap::DiamondTrap(DiamondTrap const &src) : ClapTrap(src.getname()), FragTrap(src), ScavTrap(src)
{
	*this = src;
}
DiamondTrap::~DiamondTrap(void)
{
	std::cout << "Default DiamondTrap destructor called" << std::endl;
}

DiamondTrap &	DiamondTrap::operator=(DiamondTrap const &rhs)
{
	this->FragTrap::operator=(rhs);
	this->ScavTrap::operator=(rhs);
	this->_name = rhs.getname();
	return *this;
}
std::ostream	&operator<<(std::ostream &o, DiamondTrap const &rhs)
{
	o<< "\""<<rhs.getname()<<"\"\thp="<<rhs.gethp()<<"\tep="<<rhs.getep()<<"\tdmg="<<rhs.getdmg();
	return o;
}
std::string		DiamondTrap::getname(void) const { return this->_name; }

void	DiamondTrap::whoAmI()
{
	std::cout << "My name is :\"" << this->_name << "\" but my claptrap name is :\"" << ClapTrap::getname() <<"\""<<std::endl;
	return ;
}