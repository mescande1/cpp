/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 14:38:37 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/05 01:54:50 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap : virtual public ClapTrap {
	public:
		ScavTrap(std::string nameo = "");
		ScavTrap(ScavTrap const &src);
		~ScavTrap(void);

		ScavTrap &	operator=(ScavTrap const &rhs);
		void	guardGate(void);

		void	attack(std::string const &target) const;
		bool	isguarding(void) const;

	protected:
		void	setdefaulthp(void);
		void	setdefaultep(void);
		void	setdefaultdmg(void);

	private:

		bool	_guarding;
};

std::ostream	&operator<<(std::ostream &o, ScavTrap const &rhs);

#endif