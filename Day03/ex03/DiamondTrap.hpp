/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DiamondTrap.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 17:26:51 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/05 01:52:53 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIAMONDTRAP_HPP
# define DIAMONDTRAP_HPP

# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class DiamondTrap : public FragTrap, public ScavTrap {
	public:
		DiamondTrap(std::string name = "");
		DiamondTrap(DiamondTrap const &src);
		~DiamondTrap(void);

		DiamondTrap &	operator=(DiamondTrap const &rhs);

		void	whoAmI();
		std::string		getname(void) const;

	private:

		std::string		_name;
};

std::ostream	&operator<<(std::ostream &o, DiamondTrap const &rhs);

#endif