/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 02:04:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 13:53:29 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DiamondTrap.hpp"

int main(void)
{
	DiamondTrap a("Jinx");

	a.whoAmI();
	std::cout<<a<<std::endl;

	DiamondTrap b("Vi");
	b.attack(a.getname());
	a.takeDamage(b.getdmg());
	b.beRepaired(b.getdmg());
	std::cout<<a<<std::endl;
	std::cout<<b<<std::endl;
	a = b;
	std::cout<<a<<std::endl;
	a.highFiveGuys();

	FragTrap last("Varen");
	last.attack("Vi");

	return (0);
}
