/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 14:38:01 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 10:00:33 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string name) : ClapTrap(name), _guarding(false)
{
	std::cout << "Default ScavTrap constructor called" << std::endl;
	_hp = 100;
	_ep = 50;
	_dmg = 20;
}
ScavTrap::ScavTrap(ScavTrap const &src) : ClapTrap(src), _guarding(false)
{
	std::cout << "Copy ScavTrap constructor called" << std::endl;
	*this = src;
}
ScavTrap::~ScavTrap(void)
{
	std::cout << "Default ScavTrap Destructor called" << std::endl;
}

ScavTrap &	ScavTrap::operator=(ScavTrap const &rhs)
{
	ClapTrap::operator=(rhs);
	this->_guarding = rhs._guarding;
	return *this;
}

std::ostream	&operator<<(std::ostream &o, ScavTrap const &rhs)
{
	o << "Garding"<<rhs.isguarding() <<"\t"<< ClapTrap(rhs);
	return o;
}

void	ScavTrap::guardGate(void)
{
	if (!this->_guarding)
		std::cout << "ScavTrap have enterred in Gate Keeper mode !";
	this->_guarding = true;
	return ;
}

bool	ScavTrap::isguarding(void) const { return _guarding; }
void	ScavTrap::attack(std::string const &target) const
{
	std::cout << "ScavTrap "<<getname()<<", because he must be unique, attack "<<target<<", causing "<<getdmg()<<" points of damage!"<<std::endl;
}