/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 14:38:37 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 10:08:08 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap {
	public:
		FragTrap(std::string name = "");
		FragTrap(FragTrap const &src);
		~FragTrap(void);

		FragTrap &	operator=(FragTrap const &rhs);
		void	highFiveGuys(void);


	private:
};

std::ostream	&operator<<(std::ostream &o, FragTrap const &rhs);

#endif