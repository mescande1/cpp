/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 02:04:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 10:10:05 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"

int main(void)
{
	FragTrap a;
	FragTrap b("Patrick");
	FragTrap c("Jose");
	FragTrap d(b);

	std::cout<<a<<std::endl;
	a = c;
	std::cout<<a<<std::endl;
	a.beRepaired(10);
	std::cout<<a<<std::endl;
	a.highFiveGuys();
	return (0);
}
