/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 01:44:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/16 10:07:10 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap(std::string name) : _name(name), _hp(10), _ep(10), _dmg(0)
{
	std::cout << "Default ClapTrap constructor called" << std::endl;
}
ClapTrap::ClapTrap(ClapTrap const &src) : _name(src._name), _hp(src._hp), _ep(src._ep), _dmg(src._dmg)
{
	std::cout << "Copy ClapTrap constructor called" << std::endl;
}
ClapTrap::~ClapTrap(void)
{
	std::cout << "Default ClapTrap destructor called" << std::endl;
}

ClapTrap	&ClapTrap::operator=(ClapTrap const &rhs)
{
	this->_name = rhs._name;
	this->_hp = rhs._hp;
	this->_ep = rhs._ep;
	this->_dmg = rhs._dmg;
	return *this;
}
std::ostream	&operator<<(std::ostream &o, ClapTrap const &rhs)
{
	o << "\""<<rhs.getname()<<"\"\thp="<<rhs.gethp()<<"\tep="<<rhs.getep()<<"\tdmg="<<rhs.getdmg();
	return o;
}

void	ClapTrap::attack(std::string const &target) const
{
	std::cout << "ClapTrap "<<this->_name<<" attack "<<target<<", causing "<<this->_dmg<<" points of damage!"<<std::endl;
	return ;
}

void	ClapTrap::takeDamage(unsigned int amount)
{
	std::cout << "ClapTrap "<<this->_name<<" is attacked for "<<amount<<"hp!"<<std::endl;
	if (_hp <= amount)
	{
		_hp = 0;
		return ;
	}
	_hp -= amount;
	return ;
}

void	ClapTrap::beRepaired(unsigned int amount)
{
	std::cout << "ClapTrap "<<this->_name<<" is healed for "<<amount<<"hp!"<<std::endl;
	_hp += amount;
	return ;
}

std::string		ClapTrap::getname(void) const { return this->_name; }
unsigned int	ClapTrap::gethp(void) const { return this->_hp; }
unsigned int	ClapTrap::getep(void) const { return this->_ep; }
unsigned int	ClapTrap::getdmg(void) const { return this->_dmg; }