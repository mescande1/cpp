/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 18:52:55 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 17:41:14 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Karen.hpp"

enum {
	anything,
	debug,
	info,
	warning,
	error
};

static const struct s_strint tab[4] = {
	{"DEBUG", debug},
	{"INFO", info},
	{"WARNING", warning},
	{"ERROR", error}
};

int main(int ac, char **av)
{
	if (ac != 2)
	{
		std::cout << "Usage:\n\t./karenFilter [arg]\n[arg] is whatever you want in this list or not : DEBUG, INFO, WARNING, ERROR\n";
		return (0);
	}
	Karen k;
	int	i = 0;
	while (i < 4 && ((std::string)av[1]).compare(tab[i].level))
		i++;
	switch(tab[i].cas)
	{
		case debug:
			k.complain("DEBUG");
			__attribute__((fallthrough));
		case info:
			k.complain("INFO");
			__attribute__((fallthrough));
		case warning:
			k.complain("WARNING");
			__attribute__((fallthrough));
		case error:
			k.complain("ERROR");
			break;
		default:
			std::cout << "[ Probably complaining about insignificant problems ]" << std::endl;
			break;
	}
}
