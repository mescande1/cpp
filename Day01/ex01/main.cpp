/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 11:58:07 by matthieu          #+#    #+#             */
/*   Updated: 2021/11/18 12:48:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

int		main(void)
{
	int n;
	Zombie *ptr;

	n = 12;
	ptr = zombieHorde(n, "Michel");
	for (int i = 0; i < n; i++)
	{
		std::cout << i << "\t";
		ptr[i].annouce();
	}
	delete[] ptr;
	n = 30;
	ptr = zombieHorde(n, "Tom & Jerry");
	for (int i = 0; i < n; i++)
	{
		std::cout << i << "\t";
		ptr[i].annouce();
	}
	delete[] ptr;
	return (0);
}
