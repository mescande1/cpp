/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/12 16:22:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 15:51:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

#include <iostream>

class Zombie {

	public:
		Zombie(std::string name = "");
		~Zombie(void);
		void	annouce(void) const;

	private:
		const std::string	_name;
};

void	randomChump(std::string name);
Zombie	*newZombie(std::string name);

#endif
