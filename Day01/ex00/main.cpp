/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/12 17:45:46 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/09 20:33:31 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

int main(void)
{
	std::string	name;
	Zombie		*george;

	name = "George";
	george = newZombie(name);
	george->annouce();
	name = "Adam Anders";
	randomChump(name);
	delete george;
	return (0);
}
