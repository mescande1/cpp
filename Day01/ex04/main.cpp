/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 16:12:24 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/13 18:07:59 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include <string>

#include <unistd.h>

int	main(int ac, char **av)
{
	if (ac != 4)
	{
		std::cout << "Usage:\n\t./replace [filename] [s1] [s2]\n[filename].replace is a created and content a copy of filename except that [s1] is replaced by [s2]\n";
		return 0;
	}
	std::string		filename = av[1];
	std::string		s1 = av[2];
	std::string		s2 = av[3];
	std::ifstream	src;
	std::ofstream	dst;
	std::string		buff;
	size_t			occ = 0;
	src.open(filename.data());
	if (src.fail())
	{
		std::cout<<"File couldn't be opened !"<<std::endl;
		return (1);
	}
	std::getline(src, buff, '\0');
	if (src.fail())
	{
		std::cout<<"Coudn't read file !"<<std::endl;
		return (1);
	}
	else
	{
		while ((occ = buff.find(s1, occ)) != std::string::npos)
		{
			buff.erase(occ, s1.length());
			buff.insert(occ, s2);
			occ += s2.length();
		}
		filename.append(".replace");
		dst.open(filename.data());
		if (dst.fail())
		{
			std::cout<<"Couldn't open file for writing !"<<std::endl;
			return (1);
		}
		dst << buff;
		dst.close();
	}
	src.close();
	return (0);
}
