/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/18 13:03:30 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/09 19:46:55 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int		main(void)
{
	std::string	base = "HI THIS IS BRAIN";
	std::string	*stringPTR = &base;
	std::string	&stringREF = base;

	std::cout << "Address :" << std::endl << "\tstring   :" << &base << std::endl\
		<< "\tfrom str :" << stringPTR << std::endl << "\tfrom ref :" << &stringREF << std::endl;
	std::cout << "Content :" << std::endl << "\tstring   :" << base << std::endl\
		<< "\tfrom str :" << *stringPTR << std::endl << "\tfrom ref :" << stringREF << std::endl;
	return (0);
}
