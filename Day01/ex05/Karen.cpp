/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Karen.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 22:54:13 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/09 20:18:26 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Karen.hpp"

const t_fun Karen::tab[4] = {
	{"DEBUG", &Karen::debug},
	{"INFO", &Karen::info},
	{"WARNING", &Karen::warning},
	{"ERROR", &Karen::error}};

Karen::Karen(void) {}
Karen::~Karen(void) {}

void	Karen::complain(std::string level)
{
	int i = 0;
	while (i < 4 && level.compare(this->tab[i].level))
		i++;
	if (i != 4)
		(this->*(tab[i].fn))();
	return ;
}

void	Karen::debug(void)
{
	std::cout << "Hey, welcome on this hole new live, I'm gonna be playing lot's of games, always cam on, so follow, and sub if you'd like to see me more often ! ;)" << std::endl;
	return ;
}

void	Karen::info(void)
{
	std::cout << "Sub's costs is 5$, don't hesitate !" << std::endl;
	return ;
}

void	Karen::warning(void)
{
	std::cout << "please be polite and courteous in chat, or you'll be banned" << std::endl;
	return ;
}

void	Karen::error(void)
{
	std::cout << "Ok 'somebodywashere', that's it, you've been warned, can a moderator ban him ?" << std::endl;
	return ;
}