/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Karen.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 22:54:23 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/09 20:18:22 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KAREN_HPP
# define KAREN_HPP

#include <iostream>

typedef struct s_funtab t_fun;

class Karen {
	public:
		Karen(void);
		~Karen(void);
		void	complain(std::string level);

	private:
		static const t_fun	tab[4];
		void	debug(void);
		void	info(void);
		void	warning(void);
		void	error(void);

};

struct s_funtab {
	std::string	level;
	void		(Karen::*fn)(void);
};


#endif