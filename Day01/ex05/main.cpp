/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 15:30:58 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/09 20:19:21 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Karen.hpp"

int		main(void)
{
	Karen k;
	std::cout << "Debug :\n";
	k.complain("DEBUG");
	std::cout << "\nInfo :\n";
	k.complain("INFO");
	std::cout << "\nWarning :\n";
	k.complain("WARNING");
	std::cout << "\nError :\n";
	k.complain("ERROR");
	std::cout << "\nAnything :\n";
	k.complain("ANYTHING");
	return (0);
}
