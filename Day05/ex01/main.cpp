/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 14:46:44 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/27 11:08:16 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"

int main(void)
{
	std::cout<<std::endl<< "---------EX00---------"<<std::endl;
	Bureaucrat Biggy("Biggy", 1);
	try
	{
		Biggy++;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << Biggy << std::endl;//la valeur n'a pas changé
	try
	{
		Bureaucrat small("smally", 151);
		std::cout << small << std::endl;//pas executé car j'ai throw avant
	}
	catch (std::exception& e)
	{
		std::cout << e.what() <<std::endl;
	}

	std::cout<<std::endl<< "---------EX01---------"<<std::endl;
	Form Jaune("Jaune", 15, 10);
	try
	{
		Form notok("notok", 151, 0);//je throw d'abord pour le gradelow.
	}
	catch (std::exception& e)
	{
		std::cout<<e.what()<<std::endl;
	}
	Bureaucrat promotion_needed("Lewis", 16);
	std::cout << "promotion_needed :"<<std::endl;
	promotion_needed.signForm(Jaune);//toLow
	promotion_needed++;
	promotion_needed.signForm(Jaune);//ok
	promotion_needed.signForm(Jaune);//already signed
}
