/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 17:40:59 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 17:07:26 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include "Bureaucrat.hpp"

class Form {
	public:
		Form(std::string name = "", int _sign_grade = 150, int _exec_grade = 150);
		Form(Form const &src);
		virtual ~Form(void);

		Form &	operator=(Form const &rhs);
		std::string	getName(void) const;
		bool	isSigned(void) const;
		int	get_sign_grade(void) const;
		int	get_exec_grade(void) const;
		void	beSigned(Bureaucrat const & worker);

	struct GradeTooHighException : public std::exception
	{ virtual const char* what() const throw(); };
	struct GradeTooLowException : public std::exception
	{ virtual const char* what() const throw(); };
	struct AlreadySigned : public std::exception
	{ virtual const char* what() const throw(); };

	protected:

	private:

		const std::string	_name;
		bool		_signed;
		const int	_sign_grade;
		const int	_exec_grade;
};

std::ostream	&operator<<(std::ostream &o, Form const &rhs);

#endif