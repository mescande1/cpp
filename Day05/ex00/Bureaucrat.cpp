/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 12:22:45 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/26 09:13:41 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(std::string name, int grade) : _name(name), _grade(grade)
{
	if (_grade < 1)
		throw GradeTooHighException();
	if (150 < _grade)
		throw GradeTooLowException();
}
Bureaucrat::Bureaucrat(Bureaucrat const &src) : _name(src._name) { *this = src; }
Bureaucrat::~Bureaucrat(void) {}

Bureaucrat &	Bureaucrat::operator=(Bureaucrat const &rhs)
{
	_grade = rhs._grade;
	return *this;
}

std::string	Bureaucrat::getName(void) const
{
	return _name;
}
int	Bureaucrat::getGrade(void) const
{
	return _grade;
}

Bureaucrat	&Bureaucrat::operator++()
{
	if (_grade > 1)
		this->_grade--;
	else
		throw GradeTooHighException();
	return *this;
}
Bureaucrat	Bureaucrat::operator++(int)
{
	Bureaucrat tmp = *this;
	++*this;
	return tmp;
}
Bureaucrat	&Bureaucrat::operator--()
{
	if (_grade < 150)
		this->_grade++;
	else
		throw GradeTooLowException();
	return *this;
}
Bureaucrat	Bureaucrat::operator--(int)
{
	Bureaucrat tmp = *this;
	--*this;
	return tmp;
}

const char* Bureaucrat::GradeTooHighException::what() const throw()
{ return ("Grade is too high !"); }
const char* Bureaucrat::GradeTooLowException::what() const throw()
{ return ("Grade is too low !"); }

std::ostream	&operator<<(std::ostream &o, Bureaucrat const &rhs)
{
	o << rhs.getName() << ", bureaucrat grade " << rhs.getGrade() << ".";
	return o;
}