/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 14:46:44 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/22 17:25:20 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

int main(void)
{
	Bureaucrat Biggy("Biggy", 1);
	try
	{
		Biggy++;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << Biggy << std::endl;//la valeur n'a pas changé
	try
	{
		Bureaucrat small("smally", 151);
		std::cout << small << std::endl;//pas executé car j'ai throw avant
	}
	catch (std::exception& e)
	{
		std::cout << e.what() <<std::endl;
	}
}
