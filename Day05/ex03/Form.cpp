/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 17:45:55 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 17:09:38 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

Form::Form(std::string name, int sign_grade, int exec_grade) : _name(name), _signed(false)
{
	if (sign_grade < 1)
		throw GradeTooHighException();
	if (150 < sign_grade)
		throw GradeTooLowException();
	if (exec_grade < 1)
		throw GradeTooHighException();
	if (150 < exec_grade)
		throw GradeTooLowException();
	_exec_grade = exec_grade;
	_sign_grade = sign_grade;
}
Form::Form(Form const &src) : _name(src._name), _sign_grade(src._sign_grade), _exec_grade(src._exec_grade) { *this = src; }
Form::~Form(void) {}

Form &	Form::operator=(Form const &rhs)
{
	_signed = rhs.isSigned();
	_sign_grade = rhs.get_sign_grade();
	_exec_grade = rhs.get_exec_grade();
	return *this;
}

std::string	Form::getName(void) const { return _name; }
bool	Form::isSigned(void) const { return _signed; }
int	Form::get_sign_grade(void) const { return _sign_grade; }
int	Form::get_exec_grade(void) const { return _exec_grade; }

void	Form::beSigned(Bureaucrat const & worker)
{
	if (this->isSigned())
		throw AlreadySigned();
	if (worker.getGrade() <= _sign_grade)
		_signed = true;
	else
		throw GradeTooLowException();
	std::cout<<worker<< " signs " << *this<<std::endl;
}

const char* Form::GradeTooHighException::what() const throw()
{ return ("Grade is too high !"); }
const char* Form::GradeTooLowException::what() const throw()
{ return ("Grade is too low !"); }
const char* Form::AlreadySigned::what() const throw()
{ return ("This document is already signed.."); }
const char* Form::NotSigned::what() const throw()
{ return ("This document is not signed."); }

std::ostream	&operator<<(std::ostream &o, Form const &rhs)
{
	o << rhs.getName() << "(" << rhs.isSigned() << "," << rhs.get_sign_grade() << "," << rhs.get_exec_grade() << ")";
	return o;
}

void	Form::execute(const Bureaucrat &executor) const
{
	if (executor.getGrade() > _exec_grade)
		throw GradeTooLowException();
	if (!_signed)
		throw NotSigned();
	this->Action();
}