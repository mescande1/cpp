/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/22 14:46:44 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/29 14:26:00 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

int main(void)
{
	std::cout<<std::endl<< "---------EX00---------"<<std::endl;
	Bureaucrat Biggy("Biggy", 1);
	try
	{
		Biggy++;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << Biggy << std::endl;//la valeur n'a pas changé
	try
	{
		Bureaucrat small("smally", 151);
		std::cout << small << std::endl;//pas executé car j'ai throw avant
	}
	catch (std::exception& e)
	{
		std::cout << e.what() <<std::endl;
	}

/*	//----------Commenté car Form est maintenant une classe Abstraite----------
	std::cout<<std::endl<< "---------EX01---------"<<std::endl;
	Form Jaune("Jaune", 15, 10);
	try
	{
		Form notok("notok", 151, 0);//je throw d'abord pour le gradelow.
	}
	catch (std::exception& e)
	{
		std::cout<<e.what()<<std::endl;
	}
	Bureaucrat promotion_needed("Lewis", 16);
	std::cout << "promotion_needed :"<<std::endl;
	promotion_needed.signForm(Jaune);//toLow
	promotion_needed++;
	promotion_needed.signForm(Jaune);//ok
	promotion_needed.signForm(Jaune);//already signed*/

	std::cout<<std::endl<< "---------EX02---------"<<std::endl;
	ShrubberyCreationForm tree("maison");

	Biggy.executeForm(tree);
	Biggy.signForm(tree);
	Biggy.executeForm(tree);

	RobotomyRequestForm roboooot("drill");
	Biggy.signForm(roboooot);
	Biggy.executeForm(roboooot);
/*	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
	Biggy.executeForm(roboooot);
*/
	PresidentialPardonForm confidential("Lord Mc.Picsou");
	Biggy.signForm(confidential);
	Biggy.executeForm(confidential);

	std::cout<<std::endl<< "---------EX03---------"<<std::endl;
	Intern	larbin;
	Form	*paper = larbin.makeForm("robotomy request", "Biggy");
	Biggy.signForm(*paper);
	Biggy.executeForm(*paper);
	delete paper;
}
