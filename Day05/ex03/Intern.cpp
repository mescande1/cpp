/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/29 12:34:21 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/02 14:42:43 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"

Intern::Intern(void) {}
Intern::Intern(Intern const &src) { *this = src; }
Intern::~Intern(void) {}

Intern &	Intern::operator=(Intern const &rhs)
{
	(void)rhs;
	return *this;
}

template<typename T> Form * createInstance(std::string target) { return new T(target); }
struct s_table {
	std::string	name;
	Form* (*fn)(std::string target);
};

enum {
	shrubbery,
	robotomy,
	presidential,
	last};
s_table tab[3] = {
	{"shrubbery creation", &createInstance<ShrubberyCreationForm>},
	{"robotomy request", &createInstance<RobotomyRequestForm>},
	{"presidential pardon", &createInstance<PresidentialPardonForm>}};

Form *	Intern::makeForm(std::string name, std::string target) const
{
	Form	*ret = NULL;
	int		i;

	for (i = 0; i < last; i++)
	{
		if (!tab[i].name.compare(name))
		{
			ret = tab[i].fn(target);
			break;
		}
	}
	return ret;
}