/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/27 14:04:36 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/02 14:41:01 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form("ShrubberyCreationForm", 145, 137), _target(target.append("_shrubbery")) {}
ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &src) : Form(src) { *this = src; }
ShrubberyCreationForm::~ShrubberyCreationForm(void) {}

ShrubberyCreationForm &	ShrubberyCreationForm::operator=(ShrubberyCreationForm const &rhs)
{
	this->Form::operator=(rhs);
	return *this;
}

void	ShrubberyCreationForm::Action(void) const
{
	std::ofstream	dst;
	dst.open(_target.data());
	dst << "                                                         ." <<std::endl<<
"                                              .         ;"<<std::endl<<
"                 .              .              ;%     ;;"<<std::endl<<
"                   ,           ,                :;%  %;"<<std::endl<<
"                    :         ;                   :;%;'     .,"<<std::endl<<
"           ,.        %;     %;            ;        %;'    ,;"<<std::endl<<
"             ;       ;%;  %%;        ,     %;    ;%;    ,%'"<<std::endl<<
"              %;       %;%;      ,  ;       %;  ;%;   ,%;' "<<std::endl<<
"               ;%;      %;        ;%;        % ;%;  ,%;'"<<std::endl<<
"                `%;.     ;%;     %;'         `;%%;.%;'"<<std::endl<<
"                 `:;%.    ;%%. %@;        %; ;@%;%'"<<std::endl<<
"                    `:%;.  :;bd%;          %;@%;'"<<std::endl<<
"                      `@%:.  :;%.         ;@@%;'"<<std::endl<<
"                        `@%.  `;@%.      ;@@%;"<<std::endl<<
"                          `@%%. `@%%    ;@@%;"<<std::endl<<
"                            ;@%. :@%%  %@@%;"<<std::endl<<
"                              %@bd%%%bd%%:;"<<std::endl<<
"                                #@%%%%%:;;"<<std::endl<<
"                                %@@%%%::;"<<std::endl<<
"                                %@@@%(o);  . '"<<std::endl<<
"                                %@@@o%;:(.,'"<<std::endl<<
"                            `.. %@@@o%::;"<<std::endl<<
"                               `)@@@o%::;"<<std::endl<<
"                                %@@(o)::;"<<std::endl<<
"                               .%@@@@%::;"<<std::endl<<
"                               ;%@@@@%::;."<<std::endl<<
"                              ;%@@@@%%:;;;."<<std::endl<<
"                          ...;%@@@@@%%:;;;;,.."<<std::endl;
	dst.close();
	return ;
}