/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/28 08:47:23 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/29 10:51:02 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form("PresidentialPardonForm", 25, 5), _target(target) {}
PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &src) : Form(src) { *this = src; }
PresidentialPardonForm::~PresidentialPardonForm(void) {}

PresidentialPardonForm &	PresidentialPardonForm::operator=(PresidentialPardonForm const &rhs)
{
	this->Form::operator=(rhs);
	return *this;
}

void	PresidentialPardonForm::Action(void) const
{
	std::cout << "Important news :"<<std::endl<<_target<<" has officialy been pardonned by Zafod Beeblebrox in person !"<<std::endl;
	return ;
}