/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/28 08:33:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/29 10:46:55 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"
#include <sys/time.h>

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form("RobotomyRequestForm", 72, 45), _target(target) {}
RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &src) : Form(src) { *this = src; }
RobotomyRequestForm::~RobotomyRequestForm(void) {}

RobotomyRequestForm &	RobotomyRequestForm::operator=(RobotomyRequestForm const &rhs)
{
	this->Form::operator=(rhs);
	return *this;
}

void	RobotomyRequestForm::Action(void) const
{
	std::cout << "BrrrBBBRRBBRBRrrrrrrrr" << std::endl;
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	if (ts.tv_nsec % 2 == 0)
		std::cout << _target << " has been robotomized correctly."<<std::endl;
	else
		std::cout << _target << " couldn't be robotomized.."<<std::endl;
}