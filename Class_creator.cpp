/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Class_creator.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/18 13:33:54 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/22 17:45:13 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include <string.h>

#include <algorithm>

int main(int ac, char **av)
{
	std::string		name;
	std::string		cnam;
	std::string		hnam;
	std::ofstream	cpp;
	std::ofstream	hpp;
	std::string		args;
	std::string		tmp;

	if (ac == 2)
		name = av[1];
	else if (ac != 1)
	{
		std::cout << "nop" << std::endl;
		return (1);
	}
	else
	{
		std::cout << "Class_name:";
		std::getline(std::cin, name);
	}
	cnam = name;
	hnam = name;
	cpp.open(cnam.append(".cpp"));
	hpp.open(hnam.append(".hpp"));
	cpp << "#include \"" << hnam << "\"\n\n";
	std::cout << "Constructor arguments ? [y:N]";
	std::getline(std::cin, args);
	if (!args.compare("y"))
	{
		std::cout << "what are they ? :";
		std::getline(std::cin, args);
	}
	else
		args = "nop";
	if (args.compare("nop"))
		cpp << name << "::" << name << "(" << args << ") {}\n";
	else
		cpp << name << "::" << name << "(void) {}\n";
	cpp << name << "::" << name << "(" << name << " const &src) { *this = src; }\n";
	cpp << name << "::~" << name << "(void) {}\n";
	hnam = name;
	hnam.append("_hpp");
	std::transform(hnam.begin(), hnam.end(), hnam.begin(), ::toupper);
	hpp << "#ifndef " << hnam << "\n# define " << hnam << "\n\n";
	std::cout << "library includes in Header file ? [y:N]";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "which lib ? :";
		std::getline(std::cin, tmp);
		if (tmp.find("hpp") != std::string::npos)
			hpp << "# include \"" << tmp << "\"\n";
		else
			hpp << "# include <" << tmp << ">\n";
		std::cout << "another one ? [y:N]";
		std::getline(std::cin, tmp);
	}
	hpp<<"\n";
	std::cout << "full inheritance :";
	std::getline(std::cin, tmp);
	if (!tmp.compare(""))
		hpp << "class " << name <<" {\n\tpublic:\n";
	else
		hpp << "class " << name << " : " << tmp <<" {\n\tpublic:\n";
	if (args.compare("nop"))
		hpp << "\t\t" << name << "(" << args << ");\n";
	else
		hpp << "\t\t" << name << "(void);\n";
	hpp << "\t\t" << name << "(" << name << " const &src);\n";
	hpp << "\t\tvirtual ~" << name << "(void);\n";
	hpp << "\n\t\t" << name << " &\toperator=(" << name << " const &rhs);\n";
	cpp << "\n" << name << " &\t" << name << "::operator=(" << name << " const &rhs)\n{\n\treturn *this;\n}\n";
	std::cout << "Public fonction ? [y:N]";
	std::getline(std::cin, tmp);
	name.append("::");
	while (!tmp.compare("y"))
	{
		std::cout << "return type :";
		std::getline(std::cin, tmp);
		hpp << "\t\t" << tmp << "\t";
		cpp << "\n" << tmp << "\t";
		std::cout << "proto (no function type) :";
		std::getline(std::cin, tmp);
		hpp << tmp << ";\n";
		cpp << name << tmp << "\n{\n\treturn ;\n}";
		std::cout << "another one ? [y:N]";
		std::getline(std::cin, tmp);
	}
	std::cout << "Public variable ? [y:N]";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "full declaration :";
		std::getline(std::cin, tmp);
		hpp << "\n\t\t" << tmp << ";";
		std::cout << "another one ? [y:N]";
		std::getline(std::cin, tmp);
	}
	hpp << "\n\tprotected:\n";
	std::cout << "Protected variable ? [y:N]";
	tmp = "N";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "full declaration :";
		std::getline(std::cin, tmp);
		hpp << "\n\t\t" << tmp << ";";
		std::cout << "another one ? [y:N]";
		tmp = "N";
		std::getline(std::cin, tmp);
	}
	std::cout << "Protected fonction ? [y:N]";
	tmp = "N";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "return type :";
		std::getline(std::cin, tmp);
		hpp << "\t\t" << tmp << "\t";
		cpp << "\n\n" << tmp << "\t";
		std::cout << "proto (no function type) :";
		std::getline(std::cin, tmp);
		hpp << tmp << ";\n";
		cpp << name << tmp << "\n{\n\n}";
		std::cout << "another one ? [y:N]";
		std::getline(std::cin, tmp);
	}
	hpp << "\n\tprivate:\n";
	std::cout << "Private variable ? [y:N]";
	tmp = "N";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "full declaration :";
		std::getline(std::cin, tmp);
		hpp << "\n\t\t" << tmp << ";";
		std::cout << "another one ? [y:N]";
		tmp = "N";
		std::getline(std::cin, tmp);
	}
	std::cout << "Private fonction ? [y:N]";
	tmp = "N";
	std::getline(std::cin, tmp);
	while (!tmp.compare("y"))
	{
		std::cout << "return type :";
		std::getline(std::cin, tmp);
		hpp << "\t\t" << tmp << "\t";
		cpp << "\n\n" << tmp << "\t";
		std::cout << "proto (no function type) :";
		std::getline(std::cin, tmp);
		hpp << tmp << ";\n";
		cpp << name << tmp << "\n{\n\n}";
		std::cout << "another one ? [y:N]";
		std::getline(std::cin, tmp);
	}
	hpp << "\n};\n\n#endif";
	std::cout << "Done, thanks" <<std::endl;
	return (0);
}
