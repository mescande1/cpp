/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsp.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 09:11:27 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/15 11:58:33 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Point.hpp"

Fixed area(Point p1, Point p2, Point p3)
{
	return ((p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY()));
}

bool	bsp(Point const a, Point const b, Point const c, Point const m)
{
	Fixed d1, d2, d3;

	d1 = area(m, a, b);
	d2 = area(m, b, c);
	d3 = area(m, c, a);

	return d1 * d2 > 0 && d2 * d3 > 0;
}
