/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Point.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 08:49:22 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/15 11:55:46 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Point.hpp"

Point::Point(void) : x(0), y(0) {}
Point::Point(const Fixed x, const Fixed y) : x(x), y(y) {}
Point::Point(float x, float y) : x(x), y(y) {}
Point::Point(Point const &src) : x(src.getX()), y(src.getY()) {}
Point::~Point(void) {}

Point	&Point::operator=(const Point rhs)
{
//	*const_cast<Fixed*> (&x)= rhs.getX(); //Undefined behaviour
//	*const_cast<Fixed*> (&y)= rhs.getY(); //Undefined behaviour
//
//	this->~Point(); //even worse...
//	new (this) Point(rhs); // yup, told you !
	(void)rhs;
	return *this;
}

Fixed		Point::getX(void) const { return (Fixed(this->x)); }
Fixed		Point::getY(void) const { return (Fixed(this->y)); }

Point	Point::operator+(const Point rhs) const
{
	return Point(this->x + rhs.getX(), this->y + rhs.getY());
}
Point	Point::operator-(const Point rhs) const
{
	return Point(this->x - rhs.getX(), this->y - rhs.getY());
}

Fixed	Point::det(const Point u, const Point v)
{
	return (u.getX() * v.getY()) - (u.getY() * v.getX());
}

Fixed	Point::norme(const Point a)
{
	Fixed x2 = a.getX() * a.getX();
	Fixed y2 = a.getY() * a.getY();

	if (x2 < 0)
		x2 = x2 * Fixed(-1);
	if (y2 < 0)
		y2 = y2 * Fixed(-1);
	return Fixed::sqrt(x2 + y2);
}

std::ostream	&operator<<(std::ostream &o, Point const &rhs)
{
	o << "(" << rhs.getX() << "," << rhs.getY() << ")";
	return o;
}