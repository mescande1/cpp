/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 01:46:51 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/04 01:09:48 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <cmath>

const int	Fixed::_point = 8;

Fixed::Fixed(void) : _val(0) {}
Fixed::Fixed(Fixed const &src) { *this = src; }
Fixed::Fixed(const int i) { this->_val = i<<this->_point; }
Fixed::Fixed(const float f)
{
	int		tmp = f;
	float	ftmp = f;
	this->_val = tmp<<this->_point;
	ftmp -= tmp;
	ftmp *= 256;
	tmp = roundf(ftmp);
	this->_val += tmp;
}
Fixed::~Fixed(void) {}

Fixed &	Fixed::operator=(Fixed const &rhs)
{
	this->_val = rhs.getRawBits();
	return *this;
}

int		Fixed::getRawBits(void) const { return (this->_val); }
void	Fixed::setRawBits(int const raw) { this->_val = raw; }

float	Fixed::toFloat(void) const { return ((float)this->_val / 256); }
int		Fixed::toInt(void) const { return ((int)this->_val>>this->_point); }
std::ostream	&operator<<(std::ostream &o, Fixed const &rhs)
{
	o << rhs.toFloat();
	return o;
}

//****************************************************************************//
//                        Standart Operator                                   //
//****************************************************************************//

Fixed	Fixed::operator+(Fixed const &rhs) const
{
	float f = this->toFloat() + rhs.toFloat();
	return Fixed(f);
}
Fixed	Fixed::operator+(const float f) const { return Fixed(*this + (Fixed(f))); }
Fixed	Fixed::operator+(const int i) const { return *this + Fixed(i); }

Fixed	Fixed::operator-(Fixed const &rhs) const
{
	float f = this->toFloat() - rhs.toFloat();
	return Fixed(f);
}
Fixed	Fixed::operator-(const float f) const { return *this - Fixed(f); }
Fixed	Fixed::operator-(const int i) const { return *this - Fixed(i); }

Fixed	Fixed::operator*(Fixed const &rhs) const
{
	float f = this->toFloat() * rhs.toFloat();
	return Fixed(f);
}
Fixed	Fixed::operator*(const float f) const { return *this * Fixed(f); }
Fixed	Fixed::operator*(const int i) const { return *this *Fixed(i); }

Fixed	Fixed::operator/(Fixed const &rhs) const
{
//	float f = this->toFloat() / rhs.toFloat();
//	return Fixed(f);
	return Fixed(this->_val / rhs.getRawBits());
}
Fixed	Fixed::operator/(const float f) const { return *this / Fixed(f); }
Fixed	Fixed::operator/(const int i) const { return *this / Fixed(i); }

//****************************************************************************//
//                        Comparison operator                                 //
//****************************************************************************//

bool	Fixed::operator<(const Fixed rhs) const
{
	return (this->_val < rhs.getRawBits());
}
bool	Fixed::operator<(const float f) const { return *this < Fixed(f); }
bool	Fixed::operator<(const int i) const { return *this < Fixed(i); }

bool	Fixed::operator>(const Fixed rhs) const
{
	return (this->_val > rhs.getRawBits());
}
bool	Fixed::operator>(const float f) const {return *this > Fixed(f); }
bool	Fixed::operator>(const int i) const {return *this > Fixed(i); }

bool	Fixed::operator<=(const Fixed rhs) const { return !(*this > rhs); }
bool	Fixed::operator<=(const float f) const { return !(*this > Fixed(f)); }
bool	Fixed::operator<=(const int i) const { return !(*this > Fixed(i)); }

bool	Fixed::operator>=(const Fixed rhs) const { return !(*this < rhs); }
bool	Fixed::operator>=(const float f) const { return !(*this < Fixed(f)); }
bool	Fixed::operator>=(const int i) const { return !(*this < Fixed(i)); }

bool	Fixed::operator==(const Fixed rhs) const
{
	return (this->_val == rhs.getRawBits());
}
bool	Fixed::operator==(const float f) const { return *this == Fixed(f); }
bool	Fixed::operator==(const int i) const { return *this == Fixed(i); }

bool	Fixed::operator!=(const Fixed rhs) const { return !(*this == rhs); }
bool	Fixed::operator!=(const float f) const { return !(*this == Fixed(f)); }
bool	Fixed::operator!=(const int i) const { return !(*this == Fixed(i)); }

//****************************************************************************//
//                         Increment operator                                 //
//****************************************************************************//

Fixed	&Fixed::operator++()
{
	this->_val++;
	return *this;
}
Fixed	Fixed::operator++(int)
{
	Fixed tmp = *this;
	++*this;
	return tmp;
}
Fixed	&Fixed::operator--()
{
	this->_val--;
	return *this;
}
Fixed	Fixed::operator--(int)
{
	Fixed tmp = *this;
	--*this;
	return tmp;
}

//****************************************************************************//
//                               Min-Max                                      //
//****************************************************************************//

const Fixed	&Fixed::min(const Fixed &a, const Fixed &b)
{
	Fixed c = a, d = b;
	if (c <= d)
		return a;
	return b;
}
Fixed	&Fixed::min(Fixed &a, Fixed &b)
{
	if (a <= b)
		return a;
	return b;
}
Fixed	&Fixed::max(Fixed &a, Fixed &b)
{
	if (a >= b)
		return a;
	return b;
}
const Fixed	&Fixed::max(const Fixed &a, const Fixed &b)
{
	Fixed c = a, d = b;
	if (c >= d)
		return a;
	return b;
}

Fixed	Fixed::sqrt(Fixed a)
{
	Fixed val(53);
	Fixed res(val);
	Fixed min(0);

	min++;
	while (!(res * res <= a && (res + min) * (res + min) > a) && val > 1)
	{
		val = val / 2;
		while (res * res < a)
		{
			res = res + val;
			if (res < 0)
				res = 53;
		}
		while (res * res > a)
		{
			res = res - val;
			if (res < 0)
				res = 0;
		}
	}
	if (res * res == a)
		return (Fixed(res));
	if (res * res < a)
		while (!(res * res <= a && (res + min) * (res + min) > a))
			res++;
	else
		while (!(res * res <= a && (res + min) * (res + min) > a))
			res--;
	return (Fixed(res));
}
