/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 17:32:14 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/15 11:56:34 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Point.hpp"
#include "Fixed.hpp"

bool	bsp(Point const a, Point const b, Point const c, Point const m);

struct pointset {
	Point	p;
	bool	inside;
};

int main(void)
{
	Point a;
	Point b(Fixed(20), Fixed(0));
	Point c(0.0f, 20.2f);
	//Point m(Fixed(0), Fixed(-0.008f));
	pointset m[] = {
		{Point(Fixed(0), Fixed(0)), false},
		{Point(Fixed(20), Fixed(0)), false},
		{Point(Fixed(0), Fixed(20)), false},
		{Point(Fixed(0), Fixed(0.008f)), false},
		{Point(Fixed(0.008f), Fixed(0.008f)), true},
		{Point(Fixed(1), Fixed(1)), true},
		{Point(Fixed(-1), Fixed(1)), false},
		{Point(Fixed(-1), Fixed(-1)), false}
	};
	for (pointset *i = m; i < m + sizeof(m) / sizeof(*m); i++)
	{
		if (bsp(a, b, c, i->p) == i->inside)
			std::cout << "Point:"<<i->p<<"\tis OK"<<std::endl;
		else
			std::cout << "Point:"<<i->p<<"\tis KO-------"<<std::endl;
	}
	return (0);
}
