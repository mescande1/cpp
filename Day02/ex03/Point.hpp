/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Point.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 08:54:43 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/15 11:54:03 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POINT_HPP
# define POINT_HPP

# include "Fixed.hpp"

class Point {
	public:
		Point(void);
		Point(const Fixed x, const Fixed y);
		Point(float x, float y);
		Point(Point const &src);
		~Point(void);

		Point	&operator=(const Point rhs);
		Point	operator+(const Point rhs) const;
		Point	operator-(const Point rhs) const;

		Fixed		getX(void) const;
		Fixed		getY(void) const;

		static Fixed	norme(const Point a);
		static Fixed	det(const Point u, const Point v);

	private:

		Fixed const		x;
		Fixed const		y;
};

std::ostream	&operator<<(std::ostream &o, Point const &rhs);

#endif