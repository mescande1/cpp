/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 01:46:51 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/04 01:01:07 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <cmath>

const int	Fixed::_point = 8;

Fixed::Fixed(void) : _val(0)
{
	std::cout << "Default constructor called" << std::endl;
}
Fixed::Fixed(Fixed const &src)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}
Fixed::Fixed(const int i)
{
	std::cout << "Int constructor called" << std::endl;
	this->_val = i<<this->_point;
}
Fixed::Fixed(const float f)
{
	std::cout << "Float constructor called" << std::endl;
	int		tmp = f;
	float	ftmp = f;
	this->_val = tmp<<this->_point;
	ftmp -= tmp;
	ftmp *= 256;
	tmp = roundf(ftmp);
	this->_val += tmp;
}
Fixed::~Fixed(void)
{
	std::cout << "Destructor called" << std::endl;
}

Fixed &	Fixed::operator=(Fixed const &rhs)
{
	std::cout << "Assignation operator called" << std::endl;
	this->_val = rhs._val;
	return *this;
}

int	Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called" << std::endl;
	return (this->_val);
}

void	Fixed::setRawBits(int const raw)
{
	std::cout << "setRawBits member function called" << std::endl;
	this->_val = raw;
}

float	Fixed::toFloat(void) const
{
	return ((float)this->_val / 256);
}
int		Fixed::toInt(void) const
{
	return ((int)this->_val>>this->_point);
}
std::ostream	&operator<<(std::ostream &o, Fixed const &rhs)
{
	o << rhs.toFloat();
	return o;
}