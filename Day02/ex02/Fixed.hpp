/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 01:48:18 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/04 01:02:56 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

# include <iostream>

class Fixed {
	public:
		Fixed(void);
		Fixed(Fixed const &src);
		Fixed(const int i);
		Fixed(const float f);
		~Fixed(void);

		Fixed	&operator=(Fixed const &rhs);
		int	getRawBits(void) const;
		void	setRawBits(int const raw);

		float	toFloat(void) const;
		int		toInt(void) const;

		Fixed	operator+(Fixed const &rhs);
		Fixed	operator+(const float f);
		Fixed	operator+(const int f);
		Fixed	operator-(Fixed const &rhs);
		Fixed	operator-(const float f);
		Fixed	operator-(const int f);
		Fixed	operator*(Fixed const &rhs);
		Fixed	operator*(const float f);
		Fixed	operator*(const int f);
		Fixed	operator/(Fixed const &rhs);
		Fixed	operator/(const float f);
		Fixed	operator/(const int f);

		bool	operator<(const Fixed rhs);
		bool	operator<(const float f);
		bool	operator<(const int i);
		bool	operator>(const Fixed rhs);
		bool	operator>(const float f);
		bool	operator>(const int i);
		bool	operator<=(const Fixed rhs);
		bool	operator<=(const float f);
		bool	operator<=(const int i);
		bool	operator>=(const Fixed rhs);
		bool	operator>=(const float f);
		bool	operator>=(const int i);
		bool	operator==(const Fixed rhs);
		bool	operator==(const float f);
		bool	operator==(const int i);
		bool	operator!=(const Fixed rhs);
		bool	operator!=(const float f);
		bool	operator!=(const int i);

		Fixed	&operator++();
		Fixed	operator++(int);
		Fixed	&operator--();
		Fixed	operator--(int);

		static const Fixed	&min(const Fixed &a, const Fixed &b);
		static Fixed		&min(Fixed &a, Fixed &b);
		static const Fixed	&max(const Fixed &a, const Fixed &b);
		static Fixed		&max(Fixed &a, Fixed &b);

	private:

		int					_val;
		static const int	_point;
};

std::ostream	&operator<<(std::ostream &o, Fixed const &rhs);

#endif