/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/09 22:42:47 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/09 22:58:04 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "whatever.hpp"
#include <iostream>

int		main(void)
{
	{
		int		i1= 42, i2 = 0;
		std::cout <<"i1 = "<<i1<<", i2 = "<<i2<<", min = "<< min(i1, i2)<<std::endl;
		swap(i1, i2);
		std::cout <<"i1 = "<<i1<<", i2 = "<<i2<<", max = "<< max(i1, i2)<<std::endl;
	}
	{
		float	i1 = 35.6, i2 = 2.5;
		std::cout <<"i1 = "<<i1<<", i2 = "<<i2<<", min = "<< min(i1, i2)<<std::endl;
		swap(i1, i2);
		std::cout <<"i1 = "<<i1<<", i2 = "<<i2<<", max = "<< max(i1, i2)<<std::endl;
	}
	std::cout<<std::endl<<"--------Given main-------"<<std::endl;
	{
		int a = 2;
		int b = 3;
		::swap( a, b );
		std::cout << "a = " << a << ", b = " << b << std::endl;
		std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
		std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
		std::string c = "chaine1";
		std::string d = "chaine2";
		::swap(c, d);
		std::cout << "c = " << c << ", d = " << d << std::endl;
		std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
		std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;
	}
}
