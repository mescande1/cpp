/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/09 22:33:17 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/09 22:56:22 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WHATEVER_HPP
# define WHATEVER_HPP

template<typename T>
void	swap(T &a, T &b)
{
	T c = b;
	b = a;
	a = c;
}

template<typename T>
T	min(const T& a, const T& b)
{
	if (b <= a)
		return b;
	return a;
}

template<typename T>
T	max(const T& a, const T& b)
{
	if (b >= a)
		return b;
	return a;
}

#endif
