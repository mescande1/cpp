/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/10 12:07:53 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/10 15:40:20 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP

#include <stdexcept>

template<typename T>
class Array {
	public:
		Array<T>(void) : _data(new T[0]), _size(0) {}
		Array<T>(unsigned int n) : _data(new T[n]), _size(n) {}
		Array<T>(Array &src) : _data(new T[0]), _size(src.size()) { *this = src; };
		virtual ~Array<T>(void) { delete [] _data; }

		Array<T> &	operator=(Array<T> const &rhs) {
			delete [] _data;
			_size = rhs.size();
			_data = new T[_size];
			for (unsigned int i = 0; i < _size; i++)
				_data[i] = rhs[i];
			return *this;
		}

		T &	operator[](const unsigned int &pos) const {
			if (pos >= _size)
				throw std::out_of_range("Out of range value");
			return this->_data[pos];
		}

		unsigned int	size(void) const { return _size; }

	private:
		T *_data;
		unsigned int	_size;
};

#endif
