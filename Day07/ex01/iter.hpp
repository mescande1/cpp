/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/10 11:05:34 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/10 11:57:32 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITER_HPP
# define ITER_HPP

#include <iostream>

template<typename T, typename U, typename V>
void	iter(T *tab, U len, V f)
{
	for (int i = 0; i < len; i++)
		f(tab[i]);
}

template<typename T>
void	showit(T thing)
{
	std::cout<<thing<<std::endl;
}

#endif
