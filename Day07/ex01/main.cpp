/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/10 11:05:03 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/10 11:59:05 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "iter.hpp"

int main(void)
{
	int i[5] = {1,2,3,4,5};
	iter(i, 5, showit<int>);

	std::string str[5] = {"Hi", "this", "is", "a", "test"};
	iter(str, 5, showit<std::string>);
	return 0;
}
