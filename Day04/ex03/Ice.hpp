/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 18:04:13 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 18:04:20 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICE_HPP
# define ICE_HPP

# include "AMateria.hpp"

class Ice : public AMateria {
	public:
		Ice(void);
		Ice(Ice const &src);
		virtual ~Ice(void);

		Ice &	operator=(Ice const &rhs);
		virtual AMateria*	clone() const;
		virtual void		use(ICharacter& target);

	protected:

	private:

};

#endif