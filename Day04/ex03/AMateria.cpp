/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 17:41:13 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/08 13:23:59 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"

AMateria::AMateria(std::string const &type) : type(type){}
AMateria::AMateria(AMateria const &src) { *this = src; }
AMateria::~AMateria(void) {}

AMateria &	AMateria::operator=(AMateria const &rhs)
{
	(void) rhs;
	return *this;
}

std::string const &	AMateria::getType(void) const { return this->type; }
void	AMateria::use(ICharacter& target)
{
	std::cout<<"Using "<<this->type<<" at "<<target.getName()<<std::endl;
}