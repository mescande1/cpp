/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 12:49:05 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:55:25 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(std::string name) : _name(name)
{
	for (int i = 0; i < (int)(sizeof(slot) / sizeof(*slot)); ++i)
	{
		slot[i].materia = NULL;
		slot[i].used = false;
	}
}
Character::Character(Character const &src) { *this = src; }
Character::~Character(void)
{
	for (int i = 0; i < (int)(sizeof(slot) / sizeof(*slot)); ++i)
	{
		if (slot[i].used)
			delete slot[i].materia;
		slot[i].materia = NULL;
		slot[i].used = false;
	}
}

Character &	Character::operator=(Character const &rhs)
{
	this->_name = rhs._name;
	for (int i = 0; i < (int)(sizeof(slot) / sizeof(*slot)); ++i)
	{
		if (rhs.slot[i].used)
			this->slot[i].materia = rhs.slot[i].materia->clone();
		this->slot[i].used = rhs.slot[i].used;
	}
	return *this;
}

std::string const&	Character::getName() const
{
	return (this->_name);
}
void	Character::equip(AMateria* m)
{
	for (int i = 0; i < (int)(sizeof(slot) / sizeof(*slot)); ++i)
	{
		if (!slot[i].used)
		{
			slot[i].used = true;
			slot[i].materia = m;
			return ;
		}
	}
}
void	Character::unequip(int idx)
{
	idx--;
	if (idx < 0 || idx >= 4 || !slot[idx].used)
		return ;
	delete slot[idx].materia;
	slot[idx].used=false;
}
void	Character::use(int idx, ICharacter& target)
{
	idx--;
	if (0 <= idx && idx < 4 && slot[idx].used)
		slot[idx].materia->use(target);
}
