/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 18:04:46 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/08 14:40:28 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

Cure::Cure(void) : AMateria() { type = "cure"; }
Cure::Cure(Cure const &src) : AMateria(src) { type = "cure"; *this = src; }
Cure::~Cure(void) {}

Cure &	Cure::operator=(Cure const &rhs)
{
	(void)rhs;
	return *this;
}

AMateria*	Cure::clone() const
{
	return new Cure();
}
void	Cure::use(ICharacter& target)
{
	std::cout << "* heals "<<target.getName()<<"'s wounds *"<< std::endl;
	return ;
}