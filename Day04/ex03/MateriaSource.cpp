/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:02:57 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/08 15:16:58 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"

MateriaSource::MateriaSource(void) : used(0){}
MateriaSource::MateriaSource(MateriaSource const &src) : used(0) { *this = src; }
MateriaSource::~MateriaSource(void)
{
	for (int i = 0; i < this->used; i++)
	{
		delete tab[i];
		tab[i] = NULL;
	}
	this->used = 0;
}

MateriaSource &	MateriaSource::operator=(MateriaSource const &rhs)
{
	for (int i = 0; i < rhs.used; i++)
		this->tab[i] = rhs.tab[i]->clone();
	this->used = rhs.used;
	return *this;
}

void	MateriaSource::learnMateria(AMateria* unknown)
{
	if (used >= 4)
		return ;
	this->tab[this->used] = unknown;
	this->used++;
	return ;
}
AMateria*	MateriaSource::createMateria(std::string const & type)
{
	for (int i = 0; i < this->used; i++)
		if (!tab[i]->getType().compare(type))
			return tab[i]->clone();
	return NULL;
}