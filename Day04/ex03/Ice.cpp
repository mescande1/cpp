/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 18:02:17 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/08 14:40:35 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"

Ice::Ice(void) : AMateria() { type = "ice"; }
Ice::Ice(Ice const &src) : AMateria(src) { type = "ice"; *this = src; }
Ice::~Ice(void) {}

Ice &	Ice::operator=(Ice const &rhs)
{
	(void) rhs;
	return *this;
}

AMateria*	Ice::clone() const
{
	return new Ice(*this);
}
void	Ice::use(ICharacter& target)
{
	std::cout << "* shoots an ice bolt at " << target.getName()<<"*"<<std::endl;
	return ;
}