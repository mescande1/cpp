/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 14:26:50 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:56:48 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"
#include "Character.hpp"
#include "Cure.hpp"
#include "ICharacter.hpp"
#include "Ice.hpp"
#include "IMateriaSource.hpp"
#include "MateriaSource.hpp"

int main(void)
{
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	ICharacter* me = new Character("me");

	AMateria* tmp;
	tmp = src->createMateria("ice");
	me->equip(tmp);
	tmp = src->createMateria("cure");
	me->equip(tmp);

	ICharacter* bob = new Character("bob");

	me->use(1, *bob);
	me->use(2, *bob);

	delete bob;
	delete me;

	Character brian("brian");
	tmp = src->createMateria("cure");
	brian.equip(tmp);
	tmp = src->createMateria("ice");
	brian.equip(tmp);
	tmp = src->createMateria("ice");
	brian.equip(tmp);
	tmp = src->createMateria("ice");
	brian.equip(tmp);
	Character alex("alex");
	alex = brian;
	brian.unequip(1);
	alex.use(1, brian);
	brian.use(1, alex);//unequiped so not effective !
	brian.use(2, alex);
	brian.use(3, alex);
	brian.use(4, alex);

	delete src;


	return 0;
}
