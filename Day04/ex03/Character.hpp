/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 12:49:09 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:43:44 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include "ICharacter.hpp"
# include "AMateria.hpp"

typedef struct s_slot {
	AMateria	*materia;
	bool		used;
}	t_slot;

class Character : public ICharacter {
	public:
		Character(std::string name = "");
		Character(Character const &src);
		virtual ~Character(void);

		Character &	operator=(Character const &rhs);

		virtual std::string const&	getName() const;
		virtual void	equip(AMateria* m);
		virtual void	unequip(int idx);
		virtual void	use(int idx, ICharacter& target);

	protected:
		t_slot	slot[4];

	private:
		std::string		_name;

};

#endif