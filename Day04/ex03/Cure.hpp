/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 18:06:28 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 18:06:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURE_HPP
# define CURE_HPP

# include "AMateria.hpp"

class Cure : public AMateria {
	public:
		Cure(void);
		Cure(Cure const &src);
		virtual ~Cure(void);

		Cure &	operator=(Cure const &rhs);
		virtual AMateria*	clone() const;
		virtual void		use(ICharacter& target);

	protected:

	private:

};

#endif