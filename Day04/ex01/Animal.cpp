/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:27:24 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:22:24 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Animal.hpp"

Animal::Animal(std::string type) : type(type) {std::cout<<"Dflt\tAnimal constructor"<<std::endl;}
Animal::Animal(Animal const &src)
{
	std::cout<<"Copy\tAnimal constructor"<<std::endl;
	*this = src;
}
Animal::~Animal(void) {std::cout<<"Dflt\tAnimal destructor"<<std::endl;}

Animal &	Animal::operator=(Animal const &rhs)
{
	this->type = rhs.type;
	return *this;
}

std::string	Animal::getType(void) const { return type; }

void	Animal::makeSound() const
{
	std::cout << "an animal make a sound" <<std::endl;
}