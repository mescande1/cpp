/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:59:34 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:25:57 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAT_HPP
# define CAT_HPP

# include "Animal.hpp"
# include "Brain.hpp"

class Cat : public Animal {
	public:
		Cat(void);
		Cat(Cat const &src);
		virtual ~Cat(void);

		Cat &	operator=(Cat const &rhs);
		virtual void	makeSound(void) const;

		virtual void		setbrainvalue(int index, std::string idea);
		virtual std::string	getbrainvalue(int index) const;

	protected:

	private:
		Brain	*brain;

};

#endif