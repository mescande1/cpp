/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:28:11 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:23:44 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ANIMAL_HPP
# define ANIMAL_HPP

# include "iostream"

class Animal {
	public:
		Animal(std::string type = "");
		Animal(Animal const &src);
		virtual ~Animal(void);

		std::string	getType(void) const;
		Animal &	operator=(Animal const &rhs);
		virtual void	makeSound() const;

	protected:
		std::string	type;

	private:

};

#endif