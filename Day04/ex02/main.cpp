/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 13:04:44 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:24:08 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat.hpp"
#include "Dog.hpp"
#include "Animal.hpp"

int main(void)
{
	const Animal* j = new Dog();
	Animal* i = new Cat();
	std::cout << j->getType() << " " << std::endl;
	std::cout << i->getType() << " " << std::endl;
	i->makeSound(); //will output the cat sound!
	j->makeSound();
	*i = *j;
	i->makeSound();
	std::cout << i->getType() << " " << std::endl;
	delete j;
	delete i;
	Cat deep, *src = new Cat();
	src->setbrainvalue(3, "Genious");
	deep = *src;
	delete src;
	std::cout<<deep.getbrainvalue(3)<<std::endl;

/* ************************************************************************** */
/*                            ex01                                            */
/* ************************************************************************** */
/*	std::cout << std::endl<<"ex01"<<std::endl;
	Animal *array[100];
	for (int k = 0; k < 100; k++)
	{
		if (k %2 ==0)
			array[k] = new Dog();
		else
			array[k] = new Cat();
	}
	std::cout << array[0]->getType()<<std::endl;
	std::cout << array[1]->getType()<<std::endl;
	*(array[0]) = *(array[1]);
	std::cout << array[0]->getType()<<std::endl;
	for (int k = 0; k < 100; k++)
		delete array[k];
	//deep vs shallow : https://stackoverflow.com/questions/184710/what-is-the-difference-between-a-deep-copy-and-a-shallow-copy
*/
}
