/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:59:37 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:00:33 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat.hpp"

Cat::Cat(void) : Animal("Cat")
{
	std::cout<<"Dflt\tCat constructor"<<std::endl;
	brain = new Brain();
}
Cat::Cat(Cat const &src) : Animal("cat")
{
	std::cout<<"Copy\tCat constructor"<<std::endl;
	brain = new Brain();
	*this = src;
}
Cat::~Cat(void)
{
	std::cout<<"Dflt\tCat destructor"<<std::endl;
	delete brain;
}

Cat &	Cat::operator=(Cat const &rhs)
{
	*(this->brain) = *(rhs.brain);
	this->type = rhs.type;
	return *this;
}

void	Cat::makeSound(void) const
{
	std::cout << "miaouuuuuuu" <<std::endl;
	return ;
}
void		Cat::setbrainvalue(int index, std::string idea)
{
	if (0 <= index && index <= 100)
		brain->ideas[index] = idea;
}
std::string	Cat::getbrainvalue(int index) const
{
	if (0 <= index && index <= 100)
		return brain->ideas[index];
	return NULL;
}