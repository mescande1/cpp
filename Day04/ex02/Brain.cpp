/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 14:03:09 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:22:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain(void) {std::cout<<"Dflt\tBrain constructor"<<std::endl;}
Brain::Brain(Brain const &src)
{
	std::cout<<"Copy\tBrain constructor"<<std::endl;
	*this = src;
}
Brain::~Brain(void) {std::cout<<"Dflt\tBrain destructor"<<std::endl;}

Brain &	Brain::operator=(Brain const &rhs)
{
	for (int i = 0; i < 100; i++)
		this->ideas[i] = rhs.ideas[i];
	return *this;
}
