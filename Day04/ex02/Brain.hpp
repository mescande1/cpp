/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 14:06:39 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 14:06:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRAIN_HPP
# define BRAIN_HPP

# include "iostream"

class Brain {
	public:
		Brain(void);
		Brain(Brain const &src);
		~Brain(void);

		Brain &	operator=(Brain const &rhs);

		std::string	ideas[100];
	protected:

	private:

};

#endif