/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:54:29 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:11:31 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Dog.hpp"

Dog::Dog(void) : Animal("Dog")
{
	std::cout<<"Dflt\tDog constructor"<<std::endl;
	brain = new Brain();
}
Dog::Dog(Dog const &src) : Animal("Dog")
{
	std::cout<<"Copy\tDog constructor"<<std::endl;
	brain = new Brain();
	*this = src;
}
Dog::~Dog(void)
{
	std::cout<<"Dflt\tDog destructor"<<std::endl;
	delete brain;
}

Dog &	Dog::operator=(Dog const &rhs)
{
	*(this->brain) = *(rhs.brain);
	this->type = rhs.type;
	return *this;
}

void	Dog::makeSound(void) const
{
	std::cout << "ouaf OUAF !!"<<std::endl;
	return ;
}
void		Dog::setbrainvalue(int index, std::string idea)
{
	if (0 <= index && index <= 100)
		brain->ideas[index] = idea;
}
std::string	Dog::getbrainvalue(int index) const
{
	if (0 <= index && index <= 100)
		return brain->ideas[index];
	return NULL;
}