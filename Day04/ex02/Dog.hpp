/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:54:38 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 15:11:20 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DOG_HPP
# define DOG_HPP

# include "Animal.hpp"
# include "Brain.hpp"

class Dog : public Animal {
	public:
		Dog(void);
		Dog(Dog const &src);
		virtual ~Dog(void);

		Dog &	operator=(Dog const &rhs);
		virtual void	makeSound(void) const;

		virtual void		setbrainvalue(int index, std::string idea);
		virtual std::string	getbrainvalue(int index) const;

	protected:

	private:
		Brain	*brain;

};

#endif