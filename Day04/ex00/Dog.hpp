/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:54:38 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:12:07 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DOG_HPP
# define DOG_HPP

# include "Animal.hpp"

class Dog : public Animal {
	public:
		Dog(void);
		Dog(Dog const &src);
		virtual ~Dog(void);

		Dog &	operator=(Dog const &rhs);
		virtual void	makeSound(void) const;

	protected:

	private:

};

#endif