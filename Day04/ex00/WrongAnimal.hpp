/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WrongAnimal.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:28:11 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 14:17:36 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WRONGANIMAL_HPP
# define WRONGANIMAL_HPP

# include "iostream"

class WrongAnimal {
	public:
		WrongAnimal(std::string type = "");
		WrongAnimal(WrongAnimal const &src);
		~WrongAnimal(void);

		std::string	getType(void) const;
		WrongAnimal &	operator=(WrongAnimal const &rhs);
		void	makeSound() const;

	protected:
		std::string	type;

	private:

};

#endif