/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 13:04:44 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/11 12:58:28 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat.hpp"
#include "Dog.hpp"
#include "Animal.hpp"
#include "WrongCat.hpp"
#include "WrongAnimal.hpp"

int main(void)
{
	const Animal* meta = new Animal();
	const Animal* j = new Dog();
	Animal* i = new Cat();
	std::cout << i->getType() << " " << std::endl;
	std::cout << j->getType() << " " << std::endl;
	i->makeSound(); //will output the cat sound!
	j->makeSound();
	*i = *j;
	delete j;
	std::cout << i->getType() << " " << std::endl;
	i->makeSound();
	meta->makeSound();
	delete meta;
	delete i;

	std::cout<<"-------------------------------"<<std::endl;
	const WrongAnimal*	wrongcat = new WrongCat();
	const WrongCat		okcat;
	std::cout << wrongcat->getType() << " " << std::endl;
	wrongcat->makeSound();
	std::cout << okcat.getType() << " " << std::endl;
	okcat.makeSound();
	delete wrongcat;
}
