/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WrongCat.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:59:37 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 14:00:29 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "WrongCat.hpp"

WrongCat::WrongCat(void) : WrongAnimal("WrongCat") {std::cout<<"Dflt\tWrongCat constructor"<<std::endl;}
WrongCat::WrongCat(WrongCat const &src) : WrongAnimal("cat")
{
	std::cout<<"Copy\tWrongCat constructor"<<std::endl;
	*this = src;
}
WrongCat::~WrongCat(void) {std::cout<<"Dflt\tWrongCat destructor"<<std::endl;}

WrongCat &	WrongCat::operator=(WrongCat const &rhs)
{
	this->type = rhs.type;
	return *this;
}

void	WrongCat::makeSound(void) const
{
	std::cout << "miaouuuuuuu" <<std::endl;
	return ;
}