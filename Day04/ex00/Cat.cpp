/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:59:37 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:53:06 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat.hpp"

Cat::Cat(void) : Animal("Cat") {std::cout<<"Dflt\tCat constructor"<<std::endl;}
Cat::Cat(Cat const &src) : Animal("cat")
{
	std::cout<<"Copy\tCat constructor"<<std::endl;
	*this = src;
}
Cat::~Cat(void) {std::cout<<"Dflt\tCat destructor"<<std::endl;}

Cat &	Cat::operator=(Cat const &rhs)
{
	this->type = rhs.type;
	return *this;
}

void	Cat::makeSound(void) const
{
	std::cout << "miaouuuuuuu" <<std::endl;
	return ;
}