/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:54:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:18:14 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Dog.hpp"

Dog::Dog(void) : Animal("Dog") {std::cout<<"Dflt\tDog constructor"<<std::endl;}
Dog::Dog(Dog const &src) : Animal("Dog")
{
	std::cout<<"Copy\tDog constructor"<<std::endl;
	*this = src;
}
Dog::~Dog(void) {std::cout<<"Dflt\tDog destructor"<<std::endl;}

Dog &	Dog::operator=(Dog const &rhs)
{
	this->type = rhs.type;
	return *this;
}

void	Dog::makeSound(void) const
{
	std::cout << "ouaf OUAF !!"<<std::endl;
	return ;
}