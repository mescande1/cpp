/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WrongAnimal.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 12:27:24 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/06 13:55:51 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "WrongAnimal.hpp"

WrongAnimal::WrongAnimal(std::string type) : type(type) {std::cout<<"Dflt\tWrongAnimal constructor"<<std::endl;}
WrongAnimal::WrongAnimal(WrongAnimal const &src)
{
	std::cout<<"Copy\tWrongAnimal constructor"<<std::endl;
	*this = src;
}
WrongAnimal::~WrongAnimal(void) {std::cout<<"Dflt\tWrongAnimal destructor"<<std::endl;}

WrongAnimal &	WrongAnimal::operator=(WrongAnimal const &rhs)
{
	this->type = rhs.type;
	return *this;
}

std::string	WrongAnimal::getType(void) const { return type; }

void	WrongAnimal::makeSound() const
{
	std::cout << "an animal make a sound" <<std::endl;
}