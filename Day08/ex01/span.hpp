/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <mescande@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:27:02 by mescande          #+#    #+#             */
/*   Updated: 2022/01/13 21:58:41 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP

# include <iostream>
# include <algorithm>
# include <vector>

class span {
	public:
		span(unsigned int N = 0);
		span(span const &src);
		virtual ~span(void);

		span &	operator=(span const &rhs);
		void	addNumber(int const & num);
		template<typename T>
		void	addNumber(T const &begin, T const &end) {
			if (distance(begin, end) > static_cast<long>(N - tab.size()))
				throw NotEnoughEmptySpace();
			tab.insert(tab.end(), begin, end); }
		unsigned int	shortestSpan(void) const;
		unsigned int	longestSpan(void) const;

		struct NotEnoughEmptySpace : std::exception
		{ virtual const char* what() const throw(); };
		struct NotEnoughNumbers : std::exception
		{ virtual const char* what() const throw(); };

	private:
		unsigned int		N;
		std::vector<int>	tab;
};

#endif