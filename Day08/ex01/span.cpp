/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <mescande@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:26:56 by mescande          #+#    #+#             */
/*   Updated: 2022/01/13 19:14:30 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <iterator>

span::span(unsigned int N) : N(N), tab(std::vector<int>()) {}
span::span(span const &src) : N(src.N), tab(std::vector<int>()) { *this = src; }
span::~span(void) {}

span &	span::operator=(span const &rhs)
{
	tab = rhs.tab;
	N = rhs.N;
	return *this;
}

void	span::addNumber(int const & num)
{
	if (tab.size() == N)
		throw NotEnoughEmptySpace();
	tab.push_back(num);
	return ;
}

unsigned int	span::shortestSpan(void) const
{
	if (tab.size() <= 1)
		throw NotEnoughNumbers();
	std::vector<int> v = tab;
	std::sort(v.begin(), v.end());
	std::vector<int>::iterator i = v.begin();
	unsigned int min = abs(*(i + 1) - *i);
	while (i != v.end())
	{
		if (abs(*(i + 1) - *i) < min)
			min = abs(*(i + 1) - *i);
		i++;
	}
	return min;
}
unsigned int	span::longestSpan(void) const
{
	if (tab.size() <= 1)
		throw NotEnoughNumbers();
	return *std::max_element(tab.begin(), tab.end()) - *std::min_element(tab.begin(), tab.end());
}

const char* span::NotEnoughEmptySpace::what() const throw() {return("Not Enough Empty Space");}
const char* span::NotEnoughNumbers::what() const throw() {return("Not Enough Numbers");}