/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 18:42:57 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/13 19:17:53 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <list>
#include <algorithm>

int main(int ac, char **av)
{
	if (ac == 1)
	{
		span sp = span(5);
		sp.addNumber(5);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);
		sp.addNumber(11);
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
	}
	else if (ac == 2)
	{
		unsigned int size = strtol(av[1], NULL, 10);
		span	var(size);
		srand(static_cast<unsigned int>(time(NULL)));
		std::vector<int> lst;
		for (unsigned int i = 0; i < size; i++)
			lst.push_back(i);
		std::random_shuffle(lst.begin(), lst.end());
		var.addNumber(lst.begin(), lst.end());
		std::cout << var.shortestSpan() << std::endl;
		std::cout << var.longestSpan() << std::endl;
	}
}
