/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <mescande@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 12:26:52 by mescande          #+#    #+#             */
/*   Updated: 2022/01/13 17:18:25 by mescande         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

#include <algorithm>

struct UnfinedException : std::exception
{ virtual const char* what() const throw() {return("Not Found");} };

template<typename T>
const int &	easyfind(T const &container, int const &needle)
{
	typename T::const_iterator tmp = std::find(container.begin(), container.end(), needle);
	if (tmp == container.end())
		throw UnfinedException();
	return *tmp;
}

#endif
