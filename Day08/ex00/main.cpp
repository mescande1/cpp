/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <mescande@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 12:23:52 by mescande          #+#    #+#             */
/*   Updated: 2022/01/13 17:19:39 by mescande         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <algorithm>
#include <vector>
#include "easyfind.hpp"

#define SIZE 700

int	main(void)
{
	int save = 0;
	std::vector<int> container;
	srand(static_cast<unsigned int>(time(NULL)));
	for (int i = 0; i < SIZE; ++i)
	{
		int tmp = rand();
		container.push_back(tmp);
		if (tmp % 7 == 0)
			save = tmp;
	}
	std::cout<< easyfind(container, save) <<std::endl;
	save++;
	try {
	std::cout<<easyfind(container, save)<<std::endl;
	} catch (std::exception &e) {
		std::cerr<<e.what()<<std::endl;
	}
	return 0;
}
