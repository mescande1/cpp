/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 21:34:24 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/13 22:15:31 by mescande         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.hpp"
#include <list>
#include <vector>

int main()
{
	MutantStack<int> mstack;

	mstack.push(5);
	mstack.push(17);
	mstack.push(42);
	std::cout << "top  = " << mstack.top() << std::endl;
	mstack.pop();
	std::cout << "size = " << mstack.size() << std::endl;
	mstack.push(3);
	mstack.push(5);
	mstack.push(0);

	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();

	++it;
	--it;
	std::cout << "-----print stack :"<< std::endl;
	while (it != ite)
	{
		std::cout << *it << ", ";
		++it;
	}
	std::stack<int> s(mstack);
	std::cout << std::endl << "topcopy : " << s.top() << std::endl;

	std::cout << "-------from a list-----------" <<std::endl;
	std::list<int> list;
	list.push_back(5);
	list.push_back(17);
	std::cout << "top  : " << list.back() << std::endl;
	list.pop_back();
	std::cout << "size : " << list.size() << std::endl;
	list.push_back(3);
	list.push_back(5);
	list.push_back(42);
	list.push_back(0);

	std::list<int>::iterator it_list = list.begin();
	std::list<int>::iterator ite_list = list.end();

	++it_list;
	--it_list;
	std::cout << "-----print list :"<< std::endl;
	while (it_list != ite_list)
	{
		std::cout << *it_list << ", ";
		++it_list;
	}
	std::cout << std::endl;

	std::cout << "-------from a vector-----------" <<std::endl;
	std::vector<int> vec;
	vec.push_back(5);
	vec.push_back(42);
	std::cout << "top  : " << vec.back() << std::endl;
	vec.pop_back();
	std::cout << "size : " << vec.size() << std::endl;
	vec.push_back(3);
	vec.push_back(17);
	vec.push_back(5);
	vec.push_back(0);

	std::vector<int>::iterator it_vec = vec.begin();
	std::vector<int>::iterator ite_vec = vec.end();

	++it_vec;
	--it_vec;
	std::cout << "-----print vec :"<< std::endl;
	while (it_vec != ite_vec)
	{
		std::cout << *it_vec << ", ";
		++it_vec;
	}
	std::cout << std::endl;
	return 0;
}
