/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MutantStack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:34:34 by matthieu          #+#    #+#             */
/*   Updated: 2022/01/13 21:44:35 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

# include <stack>
# include <iostream>

template<typename T>
class MutantStack : public std::stack<T> {
	public:
		MutantStack<T>(void) : std::stack<T>() {}
		MutantStack<T>(MutantStack const &src) : std::stack<T>(src) {*this = src;};
		virtual ~MutantStack<T>(void) {}

		MutantStack<T> &	operator=(MutantStack<T> const &rhs) {
			std::stack<T>::operator=(rhs);
			return *this;
		}

		typedef typename std::deque<T>::iterator iterator;
		iterator	begin() { return this->c.begin(); }
		iterator	end() { return this->c.end(); }

		typedef typename std::deque<T>::const_iterator const_iterator;
		const_iterator	begin() const { return this->c.begin(); }
		const_iterator	end() const { return this->c.end(); }
};

#endif